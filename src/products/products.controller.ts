import { Controller } from '@nestjs/common'

import { ProductsService } from './products.service'

@Controller('products')
export class ProductsController {
    constructor(private readonly productsService: ProductsService) {}

    getProducts(): Promise<string>{
        return this.productsService.getProducts()
    }
}
