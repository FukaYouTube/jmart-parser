import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import axios from 'axios'

import { Products } from './products.entity'

@Injectable()
export class ProductsService {
    constructor(@InjectRepository(Products) private products: Repository<Products>) {}

    async getProducts(): Promise<string> {

        async function getData(category_id: number = 0, page: number = 0){
            const { data } = await axios({
                method: 'GET',
                url: `https://jmart.kz/gw/catalog/v1/products?category_id=${category_id}&page=${page}&show_totals=true`,
                headers: {
                    'Contect-Type': 'application/json'
                },
                timeout: 1000 * 10
            })

            return data.data.products.map(product => {
                return {
                    product_id: product,
                    product: product,
                    popularity: product,
                    amount: product,
                    product_features: product,
                    image: product,
                    price: product,
                    base_price: product,
                    description: product,
                    company_item: product
                }
            })
        }

        getData(0, 1).then(product => {
            this.products.create(product)
        })

        return "Started process..."
    }

    findProducts(value): Promise<Products> {
        return this.products.findOne(value)
    }
}
