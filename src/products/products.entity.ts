import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'

@Entity('products')
export class Products {
    @PrimaryGeneratedColumn({ type: 'int' })
    id?: number

    @Column({ type: 'int', default: 0 })
    product_id: number

    @Column({ type: 'text', default: '' })
    product:            string
    
    @Column({ type: 'int', default: 0 })
    popularity:         number

    @Column({ type: 'int', default: 0 })
    amount:             number

    @Column('jsonb')
    product_features:   object[]
    
    @Column({ type: 'text', default: '' })
    image:              string

    @Column({ type: 'int', default: 0 })
    price:              number

    @Column({ type: 'int', default: 0 })
    base_price:         number

    @Column({ type: 'text', default: '' })
    description:        string

    @Column('jsonb')
    company_item:       object[]

    @CreateDateColumn({ type: 'timestamp without time zone', default: 'NOW()' })
    created_at?:        Date

    @UpdateDateColumn({ type: 'timestamp without time zone', default: 'NOW()', nullable: true })
    updated_at?:        Date
}