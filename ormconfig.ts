import { DataSource, DataSourceOptions } from 'typeorm'

export const connectionSource = new DataSource({
    migrationsTableName: 'migration',
    type: 'postgres',
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASS,
    host: process.env.POSTGRES_HOST,
    port: +process.env.POSTGRES_PORT,
    database: process.env.POSTGRES_DATABASE,
    entities: [
        './src/**/**.entity{.ts,.js}'
    ],
    migrations: [
        './src/database/migrations/*{.ts,.js}'
    ],
    synchronize: false,
    logging: true
} as DataSourceOptions)